﻿using System;
using System.IO;
using EnglishCracker.Droid;
using EnglishCracker.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileHelper))]
namespace EnglishCracker.Droid
{
    public class FileHelper : IFileHelper
    {
        public string GetLocalFilePath(string filename) 
            => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), filename);
    }
}