﻿using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Linq;

using Xamarin.Forms;

using EnglishCracker.Data;

namespace EnglishCracker.ViewModels
{
    class SetMaterialCategoryPageViewModel : BindableObject
    {
        EnglishCrackerDatabase db = ((App)Application.Current).Database;

        public SetMaterialCategoryPageViewModel()
        {
            CategoryList = new ObservableCollection<CategoryListItemViewModel>();
            foreach (var item in db.Categories.Select(x => new CategoryListItemViewModel(CategoryList, x)))
            {
                CategoryList.Add(item);
            }
            AddCommand = new Command(() => CategoryList.Add(new CategoryListItemViewModel(CategoryList, null) { CanEdit = true }));
        }

        public ObservableCollection<CategoryListItemViewModel> CategoryList { get; private set; }

        public ICommand AddCommand { get; private set; }
    }
}
