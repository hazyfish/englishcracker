﻿using System.Collections.ObjectModel;
using System.Linq;

using Xamarin.Forms;

using EnglishCracker.Data;

namespace EnglishCracker.ViewModels
{
    public class MaterialsPageViewModel : BindableObject
    {
        EnglishCrackerDatabase db = ((App)Application.Current).Database;

        public MaterialsPageViewModel()
        {
            MaterialList = new ObservableCollection<MaterialListItemViewModel>();
            MaterialList.CollectionChanged += async (sender, e) =>
            {
                if (e.OldItems != null)
                {
                    db.RemoveRange(e.OldItems.Cast<MaterialListItemViewModel>().Select(x => x.ReadingMaterial));
                }
                if (e.NewItems != null)
                {
                    await db.AddRangeAsync(e.NewItems.Cast<MaterialListItemViewModel>().Select(x => x.ReadingMaterial));
                }
            };
            foreach (var item in db.ReadingMaterials.Select(x => new MaterialListItemViewModel(MaterialList, x)))
            {
                MaterialList.Add(item);
            }
        }

        public ObservableCollection<MaterialListItemViewModel> MaterialList { get; private set; }
    }
}
