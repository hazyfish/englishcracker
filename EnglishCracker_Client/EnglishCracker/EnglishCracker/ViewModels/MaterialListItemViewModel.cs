﻿using System.Windows.Input;

using Xamarin.Forms;

using EnglishCracker.Models;
using System.Collections.ObjectModel;

namespace EnglishCracker.ViewModels
{
    public class MaterialListItemViewModel : BindableObject
    {
        public MaterialListItemViewModel(ObservableCollection<MaterialListItemViewModel> materialList, ReadingMaterial material)
        {
            ReadingMaterial = material;
            material.PropertyChanged += (sender, e) =>
            {
                switch (e.PropertyName)
                {
                    case nameof(material.Title):
                        OnPropertyChanged(nameof(Title));
                        break;
                    case nameof(material.Category):
                        OnPropertyChanged(nameof(Detail));
                        break;
                    default:
                        break;
                }
            };
            DeleteCommand = new Command(async () =>
            {
                if (await Application.Current.MainPage.DisplayAlert("Delete Material", $"Reading Material \"{Title}\" will be deleted", "Delete", "Cancel"))
                {
                    materialList.Remove(this);
                }
            });
        }

        public ReadingMaterial ReadingMaterial { get; private set; }

        public string Title => ReadingMaterial.Title;

        public string Detail => $"Category: {ReadingMaterial.Category.CategoryName}";

        public ICommand DeleteCommand { get; private set; }
    }
}
