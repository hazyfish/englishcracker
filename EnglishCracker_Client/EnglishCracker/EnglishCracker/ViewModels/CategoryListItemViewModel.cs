﻿using System.Windows.Input;
using System.Collections.ObjectModel;

using Microsoft.EntityFrameworkCore;

using Xamarin.Forms;

using EnglishCracker.Models;
using EnglishCracker.Data;

namespace EnglishCracker.ViewModels
{
    class CategoryListItemViewModel : BindableObject
    {
        EnglishCrackerDatabase db = ((App)Application.Current).Database;
        ObservableCollection<CategoryListItemViewModel> categoryList;

        public CategoryListItemViewModel(ObservableCollection<CategoryListItemViewModel> categoryList, Category category)
        {
            Category = category;
            this.categoryList = categoryList;
            EditedCategoryName = CategoryName;

            RenameCommand = new Command(async () =>
            {
                if (Category.ID == 1)
                {
                    await Application.Current.MainPage.DisplayAlert("Cannot Rename Category", $"Category \"{CategoryName}\" is the default category. It cannot be renamed.", "OK");
                }
                else
                {
                    CanEdit = true;
                }
            });

            DeleteCommand = new Command(async () =>
            {
                if (Category.ID == 1)
                {
                    await Application.Current.MainPage.DisplayAlert("Cannot Delete Category", $"Category \"{CategoryName}\" is the default category. It cannot be deleted.", "OK");
                }
                else if (await Application.Current.MainPage.DisplayAlert("Delete Category", $"Category \"{CategoryName}\" will be deleted", "Delete", "Cancel"))
                {
                    db.Categories.Remove(Category);
                    await db.SaveChangesAsync();
                    categoryList.Remove(this);
                }
            });

            SaveCommand = new Command(async () =>
            {
                if (string.IsNullOrWhiteSpace(EditedCategoryName))
                {
                    await Application.Current.MainPage.DisplayAlert("Empty Category Name", "Please enter a category name. ", "OK");
                }
                else if (EditedCategoryName.Trim() == CategoryName)
                {
                    CanEdit = false;
                }
                else if (await db.Categories.AnyAsync(x => x.CategoryName == EditedCategoryName.Trim()))
                {
                    await Application.Current.MainPage.DisplayAlert("Duplicate Category", $"Category \"{EditedCategoryName}\" already exists. ", "OK");
                }
                else
                {
                    if (Category == null)
                    {
                        Category = new Category() { CategoryName = EditedCategoryName.Trim() };
                        await db.Categories.AddAsync(Category);
                    }
                    else
                    {
                        Category = await db.Categories.FindAsync(Category.ID);
                        Category.CategoryName = EditedCategoryName.Trim();
                    }
                    await db.SaveChangesAsync();
                    OnPropertyChanged(nameof(CategoryName));
                    CanEdit = false;
                }
            });

            CancelCommand = new Command(() =>
            {
                if (Category == null)
                {
                    categoryList.Remove(this);
                }
                else
                {
                    CanEdit = false;
                    EditedCategoryName = Category.CategoryName;
                }
            });
        }

        public Category Category { get; private set; }

        public string CategoryName => Category?.CategoryName ?? "";
        
        private string editedCategoryName;

        public string EditedCategoryName
        {
            get => editedCategoryName;
            set
            {
                editedCategoryName = value;
                OnPropertyChanged();
            }
        }

        private bool canEdit = false;

        public bool CanEdit
        {
            get => canEdit;
            set
            {
                canEdit = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(CannotEdit));
            }
        }

        public bool CannotEdit => !CanEdit;

        public ICommand RenameCommand { get; private set; }
        public ICommand DeleteCommand { get; private set; }
        public ICommand SaveCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }
    }
}
