﻿using System;
using System.Threading.Tasks;
using Algorithmia;
using Newtonsoft.Json;

namespace EnglishCracker.APIs
{
    class SentenceSpliter
    {
        Client client;

        public SentenceSpliter()
        {
            string[] keys = new string[] 
            {
                "simSj+ezW2FXiraQGKFVoY+NArB1",
            };
            client = new Client(keys[new Random().Next(keys.Length)]);
        }

        public async Task<string[]> Split(string text)
        {
            return JsonConvert.DeserializeObject<string[]>(await Task.Run(() => client.algo("StanfordNLP/SentenceSplit/0.1.0").pipe<object>(text).result.ToString()));
        }
    }
}
