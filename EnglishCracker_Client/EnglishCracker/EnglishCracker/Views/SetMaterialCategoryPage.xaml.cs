﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using EnglishCracker.Models;
using EnglishCracker.ViewModels;

namespace EnglishCracker.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SetMaterialCategoryPage : ContentPage
    {
        public SetMaterialCategoryPage()
        {
            InitializeComponent();
            BindingContext = new SetMaterialCategoryPageViewModel();
        }

        public Category SelectedCategory => ((CategoryListItemViewModel)listView.SelectedItem)?.Category;

        async void OnItemSelected(object sender, EventArgs e)
        {
            await Navigation.PopAsync(true);
        }
    }
}