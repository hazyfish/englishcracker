﻿using System;
using System.Linq;

using Microsoft.EntityFrameworkCore;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using EnglishCracker.Data;
using EnglishCracker.Models;

namespace EnglishCracker.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditMaterialPage : ContentPage
    {
        EnglishCrackerDatabase db = ((App)Application.Current).Database;

        string originalText;

        public EditMaterialPage(ReadingMaterial readingMaterial)
        {
            BindingContext = readingMaterial;
            InitializeComponent();
            lblCategory.BindingContext = readingMaterial.Category;
            originalText = string.Join("\r", readingMaterial.Paragraphs.Select(para => "    " + string.Join(" ", para.Sentences.Select(x => x.Text))));
            editor.Text = originalText;
        }

        async void OnSetCategory(object sender, EventArgs e)
        {
            var page = new SetMaterialCategoryPage();
            page.Disappearing += (o, ea) =>
            {
                if (page.SelectedCategory == null)
                {
                    var t = lblCategory.BindingContext;
                    lblCategory.BindingContext = null;
                    lblCategory.BindingContext = t;
                }
                else
                {
                    lblCategory.BindingContext = null;
                    lblCategory.BindingContext = page.SelectedCategory;
                }
            };
            await Navigation.PushAsync(page, true);
        }

        async void OnSave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(editor.Text))
            {
                await DisplayAlert("Empty Content", "You should add some content for your reading material. ", "OK");
            }
            else if (string.IsNullOrWhiteSpace(txtTitle.Text))
            {
                await DisplayAlert("Empty Title", "You should give your reading material a title. ", "OK");
            }
            else if (txtTitle.Text.Trim() != ((ReadingMaterial)BindingContext).Title && await db.ReadingMaterials.AnyAsync(x => x.Title == txtTitle.Text.Trim()))
            {
                await DisplayAlert("Duplicate Title", $"Reading Material \"{txtTitle.Text.Trim()}\" already exists. Please change the title. ", "OK");
            }
            else
            {
                ((Button)sender).Text = "Saving...";
                IsEnabled = false;
                var material = await db.ReadingMaterials.FindAsync(((ReadingMaterial)BindingContext).ID);

                if (editor.Text == originalText)
                {
                    material.Title = txtTitle.Text.Trim();
                    material.Category = (Category)lblCategory.BindingContext;
                }
                else
                {
                    var editedMaterial = await ReadingMaterial.Create(txtTitle.Text.Trim(), editor.Text, (Category)lblCategory.BindingContext);
                    material.Title = editedMaterial.Title;
                    material.Paragraphs = editedMaterial.Paragraphs;
                    material.Category = editedMaterial.Category;
                }
                
                await db.SaveChangesAsync();
                await Navigation.PopModalAsync();
            }
        }

        async void OnCancel(object sender, EventArgs e)
        {
            if (editor.Text == originalText)
            {
                await Navigation.PopModalAsync();
            }
            else
            {
                if (await DisplayAlert("Unsaved Material", "This reading material will not be saved if you exit this page. ", "Exit", "Back to Edit"))
                {
                    await Navigation.PopModalAsync();
                }
            }
        }

        void editor_TextChanged(object sender, EventArgs e)
        {
            WordCountLabel.Text = "Word Count: " + editor.Text.Split(new string[] { " ", "\n", "\r" }, StringSplitOptions.RemoveEmptyEntries).Length;
            ParagraphCountLabel.Text = "Paragraph Count: " + editor.Text.Split(new string[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries).Length;
        }
    }
}