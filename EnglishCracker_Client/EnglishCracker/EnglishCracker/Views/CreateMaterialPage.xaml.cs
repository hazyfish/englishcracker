﻿using System;
using System.Linq;
using System.Collections.ObjectModel;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using EnglishCracker.Data;
using EnglishCracker.Models;
using EnglishCracker.ViewModels;

namespace EnglishCracker.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CreateMaterialPage : ContentPage
    {
        EnglishCrackerDatabase db = ((App)Application.Current).Database;
        ObservableCollection<MaterialListItemViewModel> materialList;

        public CreateMaterialPage(ObservableCollection<MaterialListItemViewModel> materialList)
		{
            this.materialList = materialList;
			InitializeComponent ();
            lblCategory.BindingContext = db.Categories.First();
        }

        async void OnSetCategory(object sender, EventArgs e)
        {
            var page = new SetMaterialCategoryPage();
            page.Disappearing += (o, ea) =>
            {
                if (page.SelectedCategory == null)
                {
                    lblCategory.BindingContext = db.Categories.First();
                }
                else
                {
                    lblCategory.BindingContext = null;
                    lblCategory.BindingContext = page.SelectedCategory;
                }
            };
            await Navigation.PushAsync(page, true);
        }

        async void OnCreate(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(editor.Text))
            {
                await DisplayAlert("Empty Content", "You should add some content for your reading material. ", "OK");
            }
            else if (string.IsNullOrWhiteSpace(txtTitle.Text))
            {
                await DisplayAlert("Empty Title", "You should give your reading material a title. ", "OK");
            }
            else
            {
                ((Button)sender).Text = "Creating...";
                IsEnabled = false;
                materialList.Add(new MaterialListItemViewModel(materialList, await ReadingMaterial.Create(txtTitle.Text.Trim(), editor.Text, (Category)lblCategory.BindingContext)));
                await Navigation.PopModalAsync(true);
            }
        }

        async void OnCancel(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(editor.Text))
            {
                await Navigation.PopModalAsync(true);
            }
            else
            {
                if (await DisplayAlert("Unsaved Material", "Your reading material will not be saved if you exit this page. ", "Exit", "Back to Edit"))
                {
                    await Navigation.PopModalAsync();
                }
            }
        }

        void editor_TextChanged(object sender, EventArgs e)
        {
            WordCountLabel.Text = "Word Count: " + editor.Text.Split(new string[] { " ", "\n" , "\r" }, StringSplitOptions.RemoveEmptyEntries).Length;
            ParagraphCountLabel.Text = "Paragraph Count: " + editor.Text.Split(new string[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries).Length;
        }
	}
}