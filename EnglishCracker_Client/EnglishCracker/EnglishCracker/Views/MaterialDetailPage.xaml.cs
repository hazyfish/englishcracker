﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using EnglishCracker.Models;

namespace EnglishCracker.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MaterialDetailPage : ContentPage
	{
        Action deleteMaterial;

        public MaterialDetailPage (ReadingMaterial readingMaterial, Action deleteMaterial)
        {
            BindingContext = readingMaterial;
            this.deleteMaterial = deleteMaterial;
			InitializeComponent();
		}

        async void OnStartReading(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ReadingPage((ReadingMaterial)BindingContext, (int)sldReadingSpeed.Value), true);
        }

        void OnReadingSpeedChanged(object sender, EventArgs e)
        {
            sldReadingSpeed.Value = Math.Round(sldReadingSpeed.Value / 10) * 10;
        }

        async void OnEdit(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new EditMaterialPage((ReadingMaterial)BindingContext)), true);
        }

        async void OnDelete(object sender, EventArgs e)
        {
            var material = (ReadingMaterial)BindingContext;
            if (await DisplayAlert("Delete Material", $"Reading Material \"{material.Title}\" will be deleted", "Delete", "Cancel"))
            {
                deleteMaterial();
                await Navigation.PopAsync(true);
            }
        }
    }
}