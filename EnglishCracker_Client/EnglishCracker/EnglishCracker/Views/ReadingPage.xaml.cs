﻿using System;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using EnglishCracker.Models;

namespace EnglishCracker.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReadingPage : ContentPage
    {
        private int readingSpeed;

        public ReadingPage(ReadingMaterial readingMaterial, int readingSpeed)
        {
            BindingContext = readingMaterial;
            this.readingSpeed = readingSpeed;
            InitializeComponent();
        }

        async void OnAppearing(object sender, EventArgs e)
        {
            void AppendText(string text) => formattedString.Spans.Add(new Span() { Text = text, FontSize = 16, ForegroundColor = Color.Gray});

            foreach (var paragraph in ((ReadingMaterial)BindingContext).Paragraphs)
            {
                AppendText("    ");
                foreach (var sentence in paragraph.Sentences)
                {
                    AppendText(sentence.Text + " ");
                }
                AppendText(Environment.NewLine);
            }

            foreach (Span span in formattedString.Spans)
            {
                span.ForegroundColor = Color.Blue;
                await Task.Delay((int)(span.Text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Length / (readingSpeed / 60.0) * 1000));
                span.ForegroundColor = Color.Black;
            }
        }
    }
}