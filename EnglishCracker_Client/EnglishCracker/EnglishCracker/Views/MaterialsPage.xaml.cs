﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using EnglishCracker.Data;
using EnglishCracker.ViewModels;

namespace EnglishCracker.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MaterialsPage : ContentPage
	{
        EnglishCrackerDatabase db = ((App)Application.Current).Database;

        public MaterialsPage ()
        {
            InitializeComponent ();
            BindingContext = new MaterialsPageViewModel();
        }
        
        async void OnAdd(object sender, EventArgs e)
        {
            var page = new NavigationPage(new CreateMaterialPage(((MaterialsPageViewModel)BindingContext).MaterialList));
            await Navigation.PushModalAsync(page);
        }

        async void OnItemSelected(object sender, EventArgs e)
        {
            var materialVM = (MaterialListItemViewModel)((ListView)sender).SelectedItem;
            var material = materialVM?.ReadingMaterial;
            ((ListView)sender).SelectedItem = null;
            if (material != null && Navigation.NavigationStack.Count == 1) //Compromise for a Bug in Xamarin Forms UWP
            {
                var page = new MaterialDetailPage(material, () => ((MaterialsPageViewModel)BindingContext).MaterialList.Remove(materialVM));
                await Navigation.PushAsync(page, true);
            }
        }
    }
}