﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EnglishCracker.Interfaces
{
    public interface IFileHelper
    {
        string GetLocalFilePath(string fileName);
    }
}
