﻿using EnglishCracker.Data;
using EnglishCracker.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EnglishCracker
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            db = EnglishCrackerDatabase.Create(DependencyService.Get<IFileHelper>().GetLocalFilePath("database.db")).Result;
            MainPage = new NavigationPage(new EnglishCracker.Views.MainPage());
        }

        private EnglishCrackerDatabase db;

        public EnglishCrackerDatabase Database => db;

        protected override void OnStart()
        {

        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
