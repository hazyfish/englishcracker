﻿using EnglishCracker.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace EnglishCracker.Data
{
    public class EnglishCrackerDatabase : DbContext
    {
        public DbSet<ReadingMaterial> ReadingMaterials { get; set; }

        public DbSet<Category> Categories { get; set; }

        public async static Task<EnglishCrackerDatabase> Create(string databasePath)
        {
            var dbContext = new EnglishCrackerDatabase(databasePath);
            dbContext.Database.Migrate();
            if (!await dbContext.Categories.AnyAsync())
            {
                await dbContext.Categories.AddAsync(new Category() { CategoryName = "Uncategorized" });
                await dbContext.SaveChangesAsync();
            }
            return dbContext;
        }

        protected string DatabasePath { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ReadingMaterial>().Property("jsonContent");
            modelBuilder.Entity<ReadingMaterial>().Property("categoryID");
        }

        protected EnglishCrackerDatabase(string databasePath)
        {
            DatabasePath = databasePath;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Filename={DatabasePath}");
        }
    }
}
