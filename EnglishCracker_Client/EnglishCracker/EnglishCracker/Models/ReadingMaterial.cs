﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Xamarin.Forms;

using EnglishCracker.APIs;
using EnglishCracker.Data;

namespace EnglishCracker.Models
{
    public class ReadingMaterial : BindableModel
    {
        EnglishCrackerDatabase db = ((App)Application.Current).Database;

        [Key]
        public int ID { get; set; }

        public DateTime AddingTime { get; private set; }
        
        private string title;
        public string Title
        {
            get => title;
            set
            {
                title = value;
                OnPropertyChanged();
            }
        }

        private int categoryID;

        [NotMapped]
        public Category Category
        {
            get => db.Categories.Find(categoryID);
            set
            {
                categoryID = value.ID;
                OnPropertyChanged();
            }
        }

        private string jsonContent;

        private List<Paragraph> paragraphs;

        [NotMapped]
        public List<Paragraph> Paragraphs
        {
            get
            {
                if (paragraphs == null)
                {
                    paragraphs = JsonConvert.DeserializeObject<List<Paragraph>>(jsonContent);
                }
                return paragraphs;
            }

            set
            {
                jsonContent = JsonConvert.SerializeObject(value);
                paragraphs = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(WordCount));
            }
        }

        [NotMapped]
        public int WordCount => Paragraphs.Sum(x => x.WordCount);

        public static async Task<ReadingMaterial> Create(string title, string content, Category category)
        {
            ReadingMaterial readingMaterial = new ReadingMaterial() { Title = title, AddingTime = DateTime.Now, Category = category };
            SentenceSpliter sentenceSpliter = new SentenceSpliter();
            readingMaterial.Paragraphs = (await Task.WhenAll(content.Split(new string[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries).Select(async para => new Paragraph() { Sentences = (await sentenceSpliter.Split(para)).Select(sentence => new Sentence() { Text = sentence }).ToList() }))).ToList();
            return readingMaterial;
        }
    }
}
