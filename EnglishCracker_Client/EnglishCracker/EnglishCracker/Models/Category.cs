﻿using System.ComponentModel.DataAnnotations;

namespace EnglishCracker.Models
{
    public class Category
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string CategoryName { get; set; }
    }
}
