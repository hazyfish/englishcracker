﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace EnglishCracker.Models
{
    public abstract class BindableModel : INotifyPropertyChanged
    {
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
