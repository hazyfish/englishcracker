﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace EnglishCracker.Models
{
    public class Sentence
    {
        public string Text { get; set; }

        [JsonIgnore]
        public int WordCount => Text.Split(' ').Length;
    }
}
