﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EnglishCracker.Models
{
    public class Paragraph
    {
        public List<Sentence> Sentences { get; set; }

        [JsonIgnore]
        public int WordCount => Sentences.Sum(x => x.WordCount);
    }
}
