﻿using EnglishCracker.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace EnglishCracker.Data
{
    public class EnglishCrackerDatabase : DbContext
    {
        public DbSet<ReadingMaterial> ReadingMaterials { get; set; }

        public DbSet<Category> Categories { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ReadingMaterial>().Property("jsonContent");
            modelBuilder.Entity<ReadingMaterial>().Property("categoryID");
        }

        public EnglishCrackerDatabase()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("placeholder.placeholder");
        }
    }
}
